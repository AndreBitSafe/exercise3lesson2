package com.lessons;

public class Main {

    public static void main(String[] args) {
        String myText = "Объект — это сущность, экземпляр класса, которой можно посылать\n " +
                "сообщения и которая может на них реагировать, используя свои данные.\n " +
                "Данные объекта скрыты от остальной программы. Сокрытие данных называется инкапсуляцией.\n";
        System.out.println(myText);

        String[] sentences = myText.
                replace("!", ".").
                replace("?", ".").
                replace("...", ".").
                split("\\.");

        String[] words = myText.replace(" -", "").
                replace(" —", "")
                .split(" ");

        int sentencesValSize = sentences.length;
        int wordsValSize = words.length;


        System.out.println("Количество предложений: " + sentencesValSize);
        System.out.println("Количество слов: " + wordsValSize);
    }
}
